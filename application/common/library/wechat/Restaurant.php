<?php

namespace app\common\library\wechat;

use app\common\library\UtilityTool;
use think\Cache;
use think\Db;

/**
 * 点餐 - 微信小程序
 */
class Restaurant
{
   
    // 小程序数据
    const APPID = 'wx240dd5b62a5eac77';
    const APPSECRT = '6ce6665720c329187006ae8efe256fc7';

    /**
     * 解密微信用户信息
     *
     * @param   string    $url    请求路径
     * @return  Array
     */
    static function wechatDecode($code,$encryptedData,$iv)
    {
    	$wechatDecode = new \wechat\miniprogramdecode\wxBizDataCrypt(self::APPID,self::getSessionKey($code));
        $errCode = $wechatDecode->decryptData($encryptedData, $iv, $data );
        if ($errCode == 0) {
        	$userInfo = json_decode($data,true);
        	$isset = Db::table('fa_customer')->where('openid',$userInfo['openId'])->value('openId');
        	if(!$isset){
        		unset($userInfo['watermark']);
                $userInfo['created_at'] = date('Y-m-d H:i:s',time());
        		Db::table('fa_customer')->insert($userInfo);
        	}else{
                $isset = Db::table('fa_customer')->where('openid',$userInfo['openId'])->setInc('phone');
            }
            return $userInfo;

        } else {
            return $errCode;
        }
    }

    /**
     * 获取微信session_Key
     *
     * @param   string    $code    登录凭证
     * @return  Array
     */
    private static function getSessionKey($code)
    {
    	// session_key在有效期
    	// $sessionKey = Cache::get('session_key');
    	// if($sessionKey){
    	// 	return $sessionKey; 
    	// }

        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.self::APPID.'&secret='.self::APPSECRT.'&js_code='.$code.'&grant_type=authorization_code';
        $session_key = json_decode(UtilityTool::RequestGet($url),true);

        // 存入redis
        // Cache::set('session_key',$session_key['session_key'],7190);
        // Cache::set('openid2',$session_key['openid'],7190);

        // 防止报错
        if(isset($session_key['session_key'])){
        	return $session_key['session_key']; 
        }
        return false; 

    }

    /**
    * 发送post请求
    *
    * @param   string  $url         路径url
    * @param   array   $post_data   请求数据
    * @return  Array
    */
    public function RequestPost($url, $post_data){
        // header传送格式
        $headers = array(
        );
        //初始化
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, false);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, true);
        // 设置post请求参数
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        // CURLINFO_HEADER_OUT选项可以拿到请求头信息
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        //执行命令
        $data = curl_exec($curl);
        // 打印请求头信息
        // echo curl_getinfo($curl, CURLINFO_HEADER_OUT);
        //关闭URL请求
        curl_close($curl);
        //显示获得的数据
        return $data;
    }


}
