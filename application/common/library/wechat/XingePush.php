<?php

namespace app\common\library\wechat;

use app\common\library\UtilityTool;
use think\Cache;
use think\Db;

/**
 * 腾讯信鸽推送类
 */
class XingePush
{
   
    // 小程序数据
    const URL = 'https://openapi.xg.qq.com/v3/push/app';
    const AECRETKEY = 'f2f25d6ccc498188ebb9e76497c4504c';
    const ACCESSID = '2100318998';
    const APPID = 'e0f39f507742e';

    /**
     * 下发订单通知到商户
     *
     * @param   int       $store     商户id
     * @param   string    $$price    通知价格
     * @return  Array
     */
    static function broadcastNewOrders($store,$price)
    {
        // 获取商户的登陆名称
        $pushIds = Db::table('fa_user')->where('storeid',$store)->column('username');
        // dump(array_column($pushfIds,0));
        if(!$pushIds){
            return false;
        }
        // dump($pushIds);exit;
        $push = new \wechat\XingeApp(self::APPID,self::AECRETKEY);
        $mess = new \wechat\Message();
        $mess->setType(\wechat\Message::TYPE_NOTIFICATION);
        $time = date('Y-m-d H:i:ss');
        $mess->setTitle("新订单提示");
        $mess->setContent($time."您有一笔新订单，单价".$price);
        $mess->setCustom(1);
        $mess->setExpireTime(86400);
        $mess->setSendTime($time);
         
        return $push->PushAccountList($pushIds, $mess);
    }


}
