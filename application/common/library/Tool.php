<?php

namespace app\common\library;
use think\Config;
use think\Session;

class Tool
{

    // protect/ed $allowFields = ['id', 'username', 'nickname', 'mobile', 'avatar', 'score'];

    /**
     * 获取当前管理员id
     * @param array $options 参数
     * @return Auth
     */
    public static function getAdminId()
    {
        return Session::get('admin_id');
    }

  

}
