<?php
namespace app\common\library\pay;
use wechatPay\log;
use wechatPay\WxPayJsApiPay;
use wechatPay\WxPayConfig;
use wechatPay\WxPayApi;

// require_once "../lib/WxPay.Api.php";
// require_once "WxPay.JsApiPay.php";
// require_once "WxPay.Config.php";
// require_once 'log.php';
/**
 * 微信支付
 * 
 */
class WechatPay
{   

    /**
     * ②、统一下单
     * merchant_class
     * @param int $ClassId 分类id
     * @return Array
     */
    public static function getUnifiedorder($openId,$orderInfo = null)
    {
        try{
            // $tools = new \JsApiPay();
            // $openId = $tools->GetOpenid();
            //
            $input = new \wechatPay\WxPayUnifiedOrder();
            $input->SetBody("test");
            $input->SetAttach("test");
            $input->SetOut_trade_no("sdkphp".date("YmdHis"));
            $input->SetTotal_fee("1");
            $input->SetTime_start(date("YmdHis"));
            $input->SetTime_expire(date("YmdHis", time() + 600));
            $input->SetGoods_tag("test");
            $input->SetNotify_url("http://paysdk.weixin.qq.com/notify.php");
            $input->SetTrade_type("JSAPI");
            $input->SetOpenid($openId);
            $config = new \wechatPay\WxPayConfig();
            $order = WxPayApi::unifiedOrder($config, $input);
            echo '<font color="#f00"><b>统一下单支付单信息</b></font><br/>';
            return $order;
            // $jsApiParameters = $tools->GetJsApiParameters($order);

            //获取共享收货地址js函数参数
            // $editAddress = $tools->GetEditAddressParameters();
        } catch(Exception $e) {
            return Log::ERROR(json_encode($e));
        }
    }




}
