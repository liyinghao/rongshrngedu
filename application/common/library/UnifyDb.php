<?php

namespace app\common\library;
use think\Config;
use app\common\model\ExamPiece;
use app\common\model\ExamSection;
use app\common\model\ExamRelation;
use app\common\model\ExamProblem;
use app\common\model\ExamSubject;
use think\Db;
/**
 * 项目统一数据库数据助手
 *
 **/
class UnifyDb
{
    /**
    * 初始化类库
    */
    protected function __construct()
    {
        dump(111);
        
    }


    /**
     * 获取考题库集合
     * @param int $id 试题id
     * @return Array
     */
    public static function getSubject($id = null)
    {
        $where = ['status'=>null, 'deleted_at'=>null];
        $data = Db::table('edu_exam_subject')->where($where)->field('id,title,pieceNumber')->order('sort')->select();
        return $data;
    }

    /**
     * 获取指定考题库下的篇幅集合
     * @param int $id 试题id
     * @return Array
     */
    public static function getPiece($id)
    {
        if(!$id)
            return 'id为空';

        $where = ['status'=>null, 'deleted_at'=>null, 'subjectid'=>$id];
        $data = Db::table('edu_exam_piece')->where($where)->field('id,title')->order('sort')->select();
        return $data;
    }

   /**
     * 篇幅变动,修改对应的篇幅量
     * @param $id       考卷id
     */
    public static function examPieceChangeCount($id)
    {
        // 试卷表
        $subjectModel = new ExamSubject;
        // 篇幅表
        $pieceTable   = new ExamPiece;
        // 章节表
        $sectionModel = new ExamSection;
        // 关系表
        $RelationModel = new ExamRelation;
        // 暂时使用统计，递增递减太复杂，时间不够
        // 获取试卷下的篇幅列表
        $subjectList = $pieceTable->where(['subjectid'=>$id,'deleted_at'=>null])->column('id');
        // 篇幅下的章节列表
        $sectionList = $sectionModel->where(['pieceid'=>['in',$subjectList],'deleted_at'=>null])->column('id');
        // 更新试卷下篇幅数量
        $subject                =   ExamSubject::get($id);
        $subject->pieceNumber   =   count($subjectList);
        $subject->sectionNumber =   count($sectionList);
        $subject->problemNumber =   $RelationModel->where('exampiece_id','in',$sectionList)->count();
        $subject->save();
    }

    /**
     * 试题变动，修改对应的试题量
     * @param $SectionId    章节id
     */
    public static function examRelationChangeCount($sectionId)
    {
        // 章节表
        $RelationModel = new ExamRelation;
        // 更新章节下的试题数量
        $SectionUudate                 =  ExamSection::get($sectionId);
        $SectionUudate->id             =  $sectionId;
        $SectionUudate->problemNumber  =  $RelationModel->where('exampiece_id',$sectionId)->count();
        $SectionUudate->save();
        if(!$SectionUudate->pieceid)
            return false;

        self::examSectionChangeCount($SectionUudate->pieceid);
    }

    /**
     * 章节变动，修改对应的章节量
     * @param $id       篇幅id
     */
    public static function examSectionChangeCount($id)
    {
        // 篇幅表
        $pieceTable   = new ExamPiece;
        // 章节表
        $sectionModel = new ExamSection;
        // 关系表
        $RelationModel = new ExamRelation;
        /** 临时使用 **/
         // 获取篇幅下的章节量
        $sectionList = $sectionModel->where(['pieceid'=>$id,'deleted_at'=>null])->column('id');
        // 更新篇幅下信息数据
        $piece                =   ExamPiece::get($id);
        $piece->id            =   $id;
        $piece->sectionNumber =   count($sectionList);
        $piece->problemNumber =   $RelationModel->where('exampiece_id','in',$sectionList)->count();
        $piece->save();

        // 更新篇幅信息数据
        self::examPieceChangeCount($piece->subjectid);
    }

}
