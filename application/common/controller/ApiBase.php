<?php
namespace app\common\controller;

use org\Auth;
use think\Loader;
use think\Cache;
use think\Controller;
use think\Db;
use think\Session;
use think\Request;

/**
 * Api公用基础控制器
 * Class AdminBase
 * @package app\common\controller
 */
class ApiBase extends Controller
{
    // / 请求参数
    protected $Request = null;

    protected function _initialize()
    {
        parent::_initialize();
        $this->Request = Request::instance()->post();
        // $this->Request = ['subjectId'=>3,'page'=>0];
    }

     /**
     * 错误信息返回
     *
     * @access protected
     * @param  string        $msg     返回信息
     * @param  array        $data     返回数据（沉余备用）
     * @return array
     */
    protected function errorReturn($msg, $data = null)
    {
        return  json(['code'=>0, 'msg'=> $msg])->send();
    }

    /**
     * 成功信息返回
     *
     * @access protected
     * @param  string       $msg      返回信息
     * @param  array        $data     返回数据
     * @return array
     */
    protected function succeedReturn($msg, $data = null)
    {
        return  json(['code'=>200, 'msg'=> $msg, 'data'=>$data])->send();
    }

    /**
     * 权限检查
     * @return bool
     */
    protected function checkAuth()
    {

        if (!Session::has('admin_id')) {
            $this->redirect('admin/login/index');
        }

        $module     = $this->request->module();
        $controller = $this->request->controller();
        $action     = $this->request->action();

        // 排除权限
        $not_check = ['admin/Index/index', 'admin/AuthGroup/getjson', 'admin/System/clear'];

        if (!in_array($module . '/' . $controller . '/' . $action, $not_check)) {
            $auth     = new Auth();
            $admin_id = Session::get('admin_id');
            if (!$auth->check($module . '/' . $controller . '/' . $action, $admin_id) && $admin_id != 1) {
                $this->error('没有权限','admin/login');
            }
        }
    }


}