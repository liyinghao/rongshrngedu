<?php
namespace app\common\model;

use think\Model;
/**
 * 答题记录
 **/
class ExamRecord extends Model
{
    protected $insert = ['creation_at'];

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setCreationAtAttr()
    {
        return date('Y-m-d H:i:s');
    }
}