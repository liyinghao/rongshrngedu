<?php
namespace app\common\model;

use think\Model;

class ExamPiece extends Model
{
    protected $insert = ['creation_at'];

    /**
     * 创建时间
     * @return bool|string
     */
    protected function setCreateAtAttr()
    {
        return date('Y-m-d H:i:s');
    }
}