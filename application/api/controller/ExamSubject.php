<?php
namespace app\api\controller;

use app\common\controller\ApiBase;
use app\common\model\ExamSubject AS Model;
use app\common\model\ExamPiece AS PieceModel;
use app\common\model\ExamSection AS SectionModel;
use app\common\model\ExamRelation AS RelationModel;
use app\common\model\ExamProblem AS ProblemModel;
use app\common\model\ExamRecord AS RecordModel;

/**
 * 考题Api
 * Class ExamSubject
 * @package app\api\controller
 */
class ExamSubject extends ApiBase
{
    protected $Model;
    protected function _initialize()
    {
        parent::_initialize();
        $this->Model = new Model;
        
    }

    /**
     * 试题列表
     * @return Array
     */
    public function index()
    {
        // dump($this->Request());
        $where = ['deleted_at'=>null, 'status'=>null];

        $list = $this->Model->where($where)
        ->limit($this->Request['page'],15)
        ->order('sort', 'desc')->field('id,title')->select();
        return $this->succeedReturn('加载成功！',$list);
    }

    public function getProblemGather(){
        // 试题
        $ProblemModel = new ProblemModel;
        // 篇幅
        $PieceModel = new PieceModel;
        // 章节
        $SectionModel = new SectionModel;
        // 关系
        $RelationModel = new RelationModel;

        $where = array();
        $where['deleted_at'] = null;
        $where['status']     = null;
        $where['subjectid']  = $this->Request['subjectId'];
        $list = $PieceModel->where($where)->order('sort desc')->field('id,title,subjectid')->select()->toArray();
        if(!$list)
            return $this->succeedReturn('加载成功！',null);

        foreach ($list as $key => $value) {
            unset($where['subjectid']);
            $where['pieceid'] = $value['id'];
            $sectionList = false;
            $sectionList = $SectionModel->where($where)->order('sort', 'desc')->field('id,title')->select()->toArray();
            if(!$sectionList)
                continue;
            foreach ($sectionList as $index => $item) {
                $sectionListItem = array();
                 $ids = $RelationModel->where('exampiece_id',$item['id'])->column('examproblem_id');
                 if($ids){
                    unset($where['pieceid']);
                    $where['id'] = ['in',$ids];
                    $sectionListItem = $ProblemModel->where($where)->field('id,title,all_answer,right_answer')->select()->toArray();
                    unset($where['id']);
                    if($sectionListItem){
                        foreach ($sectionListItem as $k => $v) {
                            $sectionListItem[$k]['all_answer'] = json_decode($v['all_answer']);
                        }
                    }
                 }
                 $sectionList[$index]['problem'] = $sectionListItem;
            }
            $list[$key]['section'] = $sectionList;
        }
        // dump($list);exit;
        return $this->succeedReturn('加载成功！',$list);

    }

    /**
     * 记录答题记录
     * @return Array
     */
    public function recordAnswer()
    {
        $RecordModel = new RecordModel;
        // $RecordModel->record_answer = json_encode($this->Request['submitData']);
        $RecordModel->record_answer = json_encode($this->Request['submitData']);
        $RecordModel->grade = $this->Request['score'];
        $RecordModel->subjectid = $this->Request['subjectid'];
        $RecordModel->keeptime = $this->Request['keeptime'];
        $RecordModel->userid = $this->Request['userid'];
        $as = $RecordModel->save();
        if($as){
            return $this->succeedReturn('加载成功！');
        }
        return $this->errorReturn('失败');
    }

    /**
     * 答题记录查询
     * @return Array
     */
    public function getRecordData()
    {
        $RecordModel = new RecordModel;
        $as = $RecordModel->alias('record')
            ->where('userid',$this->Request['userid'])
            ->join('edu_exam_subject','edu_exam_subject.id = record.subjectid')
            ->field('grade,record.creation_at,keeptime,edu_exam_subject.title AS subjectid')
            ->order('record.id desc')
            ->select();
        if($as){
            return $this->succeedReturn('加载成功！',$as);
        }
        return $this->errorReturn('失败');
    }

}