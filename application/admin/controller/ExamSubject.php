<?php
namespace app\admin\controller;

// use app\common\model\User as UserModel;
use app\common\controller\AdminBase;
use app\common\library\Tool;
use app\common\library\UnifyDb;
use think\Config;
use think\Db;

/**
 * 考题库主题管理
 * Class AdminUser
 * @package app\admin\controller
 */
class ExamSubject extends AdminBase
{
    protected $exam_subject;

    protected function _initialize()
    {
        parent::_initialize();
        $this->exam_subject = Db::name('ExamSubject');
    }

    /**
     * 考题库主题管理
     * @param string $keyword
     * @param int    $page
     * @return mixed
     */
    public function index($keyword = '', $page = 1)
    {
        $map = ['deleted_at'=>null];
        if ($keyword) {
            $map['title'] = ['like', "%{$keyword}%"];
        }
        $subject_list = $this->exam_subject->where($map)->order('id DESC')->paginate(15, false, ['page' => $page]);

        return $this->fetch('index', ['data' => $subject_list, 'keyword' => $keyword]);
    }

    /**
     * 添加考题库主题
     * @return mixed
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 保存考题库主题
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'Examsubject');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $data['status'] = isset($data['status'])? null: 1;
                $data['creation_at'] = date('Y-m-d H:i:s',time());
                $data['creation_form'] = Tool::getAdminId();
                if ($this->exam_subject->insert($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑考题库主题
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $subject = $this->exam_subject->find($id);

        return $this->fetch('edit', ['data' => $subject]);
    }

    /**
     * 更新考题库主题
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'Examsubject');
        // 

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $subject           = array();
                $subject['id']     = $id;
                $subject['title']  = $data['title'];
                $subject['sort']   = $data['sort'];
                $subject['status'] = isset($data['status'])? null: 1;
                // dump($subject);exit;
                
                if ($this->exam_subject->update($subject)) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除考题库主题
     * @param $id
     */
    public function delete($id)
    {
        $update = ['id'=>$id, 'deleted_at' => date('Y-m-d H:i:s',time())];
        if ($this->exam_subject->update($update)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

   
}