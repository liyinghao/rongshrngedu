<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\ExamRelation;
use app\common\model\ExamProblem AS Model;
use app\common\library\Tool;
use app\common\library\UnifyDb;
use think\Db;

/**
 * 试题管理
 * Class AdminUser
 * @package app\admin\controller
 */
class ExamProblem extends AdminBase
{
    protected $Model;

    protected function _initialize()
    {
        parent::_initialize();
        $this->Model = new Model;
        // $this->model = Db::table('edu_exam_problem');
    }

    /**
     * 试题管理
     * @param string $keyword
     * @param int    $page
     * @return mixed
     */
    public function index($keyword = '', $page = 1)
    {
        $map = ['deleted_at'=>null];
        if ($keyword) {
            $map['title'] = ['like', "%{$keyword}%"];
        }
       
        $list = $this->Model->where($map)->paginate(15, false, ['page' => $page]);
        $page = $list->render();
        $data = $list->all();
        foreach ($data as $key => $value) {
            $allanswerArray = json_decode($value['all_answer']);
            $data[$key]['all_answer'] = implode('->', $allanswerArray);
            $data[$key]['right_answer'] = $allanswerArray[(int)$value['right_answer'] - 1];
        }
        
        return $this->fetch('index', [
            'page'       =>$page,
            'data'      => $data, 
            'keyword'   => $keyword
        ]);
    }

    /**
     * 章节下的试题管理
     * @param string $keyword
     * @param int    $page
     * @return mixed
     */
    public function sectionProblem($keyword = '',$exampiece_id, $page = 1)
    {
        $map = ['edu_exam_problem.deleted_at'=>null, 'exampiece_id'=>$exampiece_id];
        $list = Db::table('edu_exam_relation')->where($map)
                ->join('edu_exam_problem','edu_exam_problem.id = edu_exam_relation.examproblem_id')
                ->paginate(15);
        $data = $list->all();

        foreach ($data as $k =>$v){
            $allanswerArray = json_decode($v['all_answer']);
            $v['all_answer'] = implode('->', $allanswerArray);
            $v['right_answer'] = $allanswerArray[(int)$v['right_answer'] - 1];
            $list[$k] = $v;
        }
        
        return $this->fetch('sectionProblem', [
            'data'=> $list, 
            'keyword'=> $keyword,
            'exampiece_id' => $exampiece_id
            ]);
    }

    /**
     * 添加考题库主题
     * @return mixed
     */
    public function add()
    {
        return $this->fetch('add', ['subject'=>UnifyDb::getSubject()]);
    }

    /**
     * 保存考题库主题
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamProblem');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $data['status'] = isset($data['status'])? null: 1;
                $data['creation_at'] = date('Y-m-d H:i:s',time());
                $data['creation_form'] = Tool::getAdminId();
                $data['all_answer'] = json_encode($data['all_answer']);
                // dump($data);exit;
                if ($this->Model->insert($data)) {
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑考题库主题
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $data = $this->Model->find($id);
        $data['all_answer'] = json_decode($data['all_answer']);
        // dump($data);
        return $this->fetch('edit', ['data' => $data]);
    }

    /**
     * 更新考题库主题
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamProblem');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $update              = $this->Model->find($data['id']);
                $update->status      = isset($data['status'])? null: 1;
                $update->all_answer  = json_encode($data['all_answer']);
                $update->right_answer= $data['right_answer'];
                $update->title       = $data['title'];
                $update->id          = $data['id'];
                if ($update->save()) {
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 章节移除试题
     * @param $id
     */
    public function removeSection($exampiece_id = null)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if(!isset($data['ids']) && !$data['ids']){
                return false;
            }
            $where = array();
            $where['examproblem_id'] = ['in',$data['ids']];
            $where['exampiece_id'] = $exampiece_id;
            // 关系表
            $relationModel = new ExamRelation;
            UnifyDb::examRelationChangeCount($exampiece_id);
            $result = $relationModel->where($where)->delete();
            if($result){
                $this->success('删除成功');
            }
            $this->error('删除失败');
        }
    }

    /**
     * 删除试题
     * @param $id
     */
    public function delete($id = null)
    {
        $update = ['id'=>$id, 'deleted_at' => date('Y-m-d H:i:s',time())];
        if ($this->Model->update($update)) {
            $delSection = Db::table('edu_exam_relation')->where('examproblem_id',$id)->column('exampiece_id');
            ExamRelation::destroy(['examproblem_id'=>$id]);
            // $delSection = Db::table('edu_exam_relation')->Distinct(true)->where('examproblem_id',$id)->column('exampiece_id');
            if($delSection){
                foreach ($delSection as $key => $value) {
                    UnifyDb::examRelationChangeCount($value);
                }
            }
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 选择已存在试题
     */
    public function selectproblem($sectionId,$page = 1)
    {
        // 获取当前章节下已存在的试题
        $relationModel = new ExamRelation;
        $relationIds = $relationModel->where('exampiece_id', $sectionId)->column('examproblem_id');
        $where = array();
        $where['deleted_at'] = null;
        $where['id'] = ['not in', $relationIds]; //去除已存在的试题
        $dataList = $this->Model->where($where)->paginate(15, false, ['page' => $page]);
        $page = $dataList->render();
        $dataList = $dataList->all();
        foreach ($dataList as $key => $value) {
            $all_answer = implode('->', json_decode($value['all_answer']));
            $dataList[$key]['all_answer'] = $all_answer;
            $dataList[$key]['right_answer'] = json_decode($value['all_answer'])[$value['right_answer'] - 1];
        }
       
        return $this->fetch('selectproblem', ['data'=>$dataList,'page'=>$page,'sectionId'=>$sectionId]);
    }

    /**
     * 保存选择的试题
     */
    public function selectproblemSave($sectionId)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $insert = array();
            foreach ($data['ids'] as $key => $value) {
                if($value){
                    $insert[$key] = ['exampiece_id'=> $sectionId, 'examproblem_id'=> $value];
                }
            }

            if (Db::table('edu_exam_relation')->insertAll($insert)) {
                // 修改对应的试题数量
                UnifyDb::examRelationChangeCount($sectionId);
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
    }

    /**
     * 考题下面的篇幅
     * @param $id
     */
    public function subjectSelectItem($id)
    {
       if(!$id)
            return false;
       return UnifyDb::getPiece($id);
    }

}