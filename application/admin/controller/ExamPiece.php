<?php
namespace app\admin\controller;

use app\common\model\ExamPiece as Model;
use app\common\controller\AdminBase;
use app\common\library\Tool;
use app\common\library\UnifyDb;
use think\Config;
// use think\Request;
use think\Db;

/**
 * 考题库篇幅管理
 * Class AdminUser
 * @package app\admin\controller
 */
class ExamPiece extends AdminBase
{
    protected $model;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new Model;
    }

    /**
     * 篇幅管理首页
     * @param string $keyword   收缩关键词
     * @param int    $page      当前分页
     * @param int    $subject   所属考试
     * @return mixed
     */
    public function index($keyword = '', $subjectid='', $page = 1)
    {
        $map = ['piece.deleted_at'=>null];
        if ($keyword) {
            $map['piece.title'] = ['like', "%{$keyword}%"];
        }
        if ($subjectid) {
            $map['piece.subjectid'] = $subjectid;
        }

        $data = $this->model->alias('piece')
                ->join('edu_exam_subject','edu_exam_subject.id = piece.subjectid')
                ->where($map)
                ->field('piece.*,edu_exam_subject.title AS subject')
                ->order('sort desc')
                ->paginate(15, false, ['page' => $page]);

        return $this->fetch('index', ['data' => $data, 'keyword' => $keyword, 'subject' => UnifyDb::getSubject(), 'subjectid'=>$subjectid]);
    }

    /**
     * 添加篇幅
     * @return mixed
     */
    public function add()
    {
        $data = array();
        $data['subject'] = UnifyDb::getSubject();
        if($this->request->param('id')){
            $data['subjectId'] = $this->request->param('id');
        }
        return $this->fetch('add', $data);
    }

    /**
     * 保存篇幅
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamPiece');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $data['status'] = isset($data['status'])? null: 1;
                $data['creation_at'] = date('Y-m-d H:i:s',time());
                $data['creation_form'] = Tool::getAdminId();
                if ($this->model->insert($data)) {
                    UnifyDb::examPieceChangeCount($data['subjectid']);
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑篇幅
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $subject = $this->model->find($id);

        return $this->fetch('edit', ['data' => $subject, 'subject'=>UnifyDb::getSubject()]);
    }

    /**
     * 更新篇幅
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamPiece');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $subject           = array();
                $subject['id']     = $id;
                $subject['title']  = $data['title'];
                $subject['sort']   = $data['sort'];
                $subject['subjectid']   = $data['subjectid'];
                $subject['status'] = isset($data['status'])? null: 1;
                $subjectid = $this->model->where('id',$id)->value('subjectid');
                if ($this->model->update($subject)) {
                    // 篇幅修改，修改对应的篇幅量
                    if($subjectid !== $subject['subjectid']){
                        UnifyDb::examPieceChangeCount($subjectid);
                        UnifyDb::examPieceChangeCount($subject['subjectid']);
                    }
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除篇幅
     * @param $id
     */
    public function delete($id)
    {
        $update = ['id'=>$id, 'deleted_at' => date('Y-m-d H:i:s',time())];
        if ($this->model->update($update)) {
            UnifyDb::examPieceChangeCount($this->model->where('id',$id)->value('subjectid'));
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }
}