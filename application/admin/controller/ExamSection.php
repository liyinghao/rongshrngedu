<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\ExamSection AS Model;
use app\common\model\ExamRelation;
use app\common\library\Tool;
use app\common\library\UnifyDb;
use think\Db;

/**
 * 考题库章节管理
 * Class AdminUser
 * @package app\admin\controller
 */
class ExamSection extends AdminBase
{
    protected $model;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = new Model;
    }

    /**
     * 考题库主题管理
     * @param string $keyword
     * @param int    $page
     * @return mixed
     */
    public function index($keyword = '', $pieceid='', $page = 1)
    {
        $map = ['section.deleted_at'=>null];
        if ($keyword) {
            $map['section.title'] = ['like', "%{$keyword}%"];
        }
        if ($pieceid) {
            $map['pieceid'] = $pieceid;
        }

        $data = $this->model->alias('section')
                ->where($map)
                ->join('edu_exam_piece','edu_exam_piece.id = section.pieceid ')
                ->join('edu_exam_subject','edu_exam_subject.id = edu_exam_piece.subjectid')
                ->order('id DESC')
                ->field('section.*,edu_exam_subject.title AS subject,edu_exam_piece.title AS piece,edu_exam_piece.id AS subjectid')
                ->paginate(15, false, ['page' => $page]);

        return $this->fetch('index', [
            'data'      => $data, 
            'keyword'   => $keyword, 
            'subject'   => UnifyDb::getSubject(), 
            'pieceid'   => $pieceid,
        ]);
    }

    /**
     * 章节页面 - 新建试题
     */
    public function newproblem($sectionId)
    {
        return $this->fetch('newproblem', ['sectionId'=>$sectionId]);
    }

    /**
     * 章节页面 - 新建试题保存
     */
    public function newproblemsave()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamProblem');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {

                $data['status'] = isset($data['status'])? null: 1;
                $data['creation_at'] = date('Y-m-d H:i:s',time());
                $data['creation_form'] = Tool::getAdminId();
                $data['all_answer'] = json_encode($data['all_answer']);
                $exampiece_id = $data['exampiece_id'];
                unset($data['exampiece_id']);
                // 试题表
                $problemModel = Db::table('edu_exam_problem');
                if ($problemModel->insert($data)) {
                    // 获取添加的试题id
                    $ProblemId = $problemModel->getLastInsID();
                    // 录入关系表
                    $ExamRelationModel = new ExamRelation();
                    $ExamRelationModel->data(['exampiece_id'=>$exampiece_id , 'examproblem_id'=>$ProblemId]);
                    if($ExamRelationModel->save()){
                        // 修改对应的试题数量
                        UnifyDb::examRelationChangeCount($exampiece_id);
                        $this->success('保存成功');
                    }
                    $this->success('保存成功,但关联失败，请手动关联');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 添加考题章节
     * @return mixed
     */
    public function add()
    {
        $data = array();
        $data['subject'] = UnifyDb::getSubject();
        if($this->request->param('id')){
            $data['pieceId'] = $this->request->param('id');
            $data['subjectid'] = Db::table('edu_exam_piece')->where('id',$this->request->param('id'))->value('subjectid');
            $data['pieces'] = UnifyDb::getPiece($data['subjectid']);

        }
        // dump($data);
        // exit;
        return $this->fetch('add', $data);
    }

    /**
     * 章节试题关系录入
     * @return mixed
     */
    public function addRelation($exam_relation)
    {
        
    }

    /**
     * 保存考题章节
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamSection');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $data['status'] = isset($data['status'])? null: 1;
                $data['creation_at'] = date('Y-m-d H:i:s',time());
                $data['creation_form'] = Tool::getAdminId();
                if ($this->model->insert($data)) {
                    // 修改对应的 章节量
                    UnifyDb::examSectionChangeCount($this->model->getLastInsID());

                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑章节
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $data = $this->model
                ->where('edu_exam_section.id',$id)
                ->join('edu_exam_piece','edu_exam_piece.id = edu_exam_section.pieceid ')
                ->join('edu_exam_subject','edu_exam_subject.id = edu_exam_piece.subjectid')
                ->field('edu_exam_section.*,edu_exam_piece.id AS piece,edu_exam_subject.id AS subject,edu_exam_piece.subjectid')
                ->find();

        return $this->fetch('edit', ['data' => $data, 'subject'=>UnifyDb::getSubject(), 'piece'=>UnifyDb::getPiece($data['subject'])]);
    }

    /**
     * 更新章节
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data            = $this->request->post();
            $validate_result = $this->validate($data, 'ExamSection');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $update           = array();
                $update['id']     = $id;
                $update['title']  = $data['title'];
                $update['sort']   = $data['sort'];
                $update['pieceid']   = $data['pieceid'];
                $update['status'] = isset($data['status'])? null: 1;
                // 查询修改之前的篇幅id
                $pieceid = $this->model->where('id',$id)->value('pieceid');
                if ($this->model->update($update)) {
                    // 修改对应的 章节量
                    if($pieceid !== $update['pieceid']){
                        UnifyDb::examSectionChangeCount($pieceid);
                        UnifyDb::examSectionChangeCount($update['pieceid']);
                    }
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除章节
     * @param $id
     */
    public function delete($id)
    {
        $update = ['id'=>$id, 'deleted_at' => date('Y-m-d H:i:s',time())];
        if ($this->model->update($update)) {
            UnifyDb::examSectionChangeCount($id);
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 考题下面的篇幅
     * @param $id
     */
    public function subjectSelectItem($id)
    {
       return UnifyDb::getPiece($id);
    }

}