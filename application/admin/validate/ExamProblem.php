<?php
namespace app\admin\validate;

use think\Validate;

class ExamProblem extends Validate
{
    protected $rule = [
        'title'         => 'require',
        'all_answer'     => 'require',
        'right_answer' => 'require'
    ];

    protected $message = [
        'title.require'         => '请输入标题',
        'right_answer.require'  => '必须选择正确答案',
        'all_answer.require'  => '必须填写可选答案'
    ];
}