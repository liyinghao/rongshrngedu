<?php
namespace app\admin\validate;

use think\Validate;

class ExamSection extends Validate
{
    protected $rule = [
        'title'         => 'require|max:24',
        'pieceid'     => 'require',
        'sort' => 'number'
    ];

    protected $message = [
        'title.require'         => '请输入标题',
        'title.max'     => '标题最多不能超过24个字符',
        'sort.number'  => '排序只能填写数字',
        'pieceid.require'  => '请选择所属篇幅'
    ];
}