<?php
namespace app\admin\validate;

use think\Validate;

class ExamPiece extends Validate
{
    protected $rule = [
        'title'         => 'require|max:24',
        'subjectid'     => 'require',
        'sort' => 'number'
    ];

    protected $message = [
        'title.require'         => '请输入标题',
        'title.max'     => '标题最多不能超过24个字符',
        // 'title.unique'          => '标题已存在',
        'sort.number'  => '排序只能填写数字'
        'subjectid.require'  => '请选择所属考试'
    ];
}