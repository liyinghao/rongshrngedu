<?php
namespace app\admin\validate;

use think\Validate;

class Examsubject extends Validate
{
    protected $rule = [
        'title'         => 'require|unique:exam_subject|max:24',
        'sort' => 'number'
    ];

    protected $message = [
        'title.require'         => '请输入标题',
        'title.max'     => '标题最多不能超过24个字符',
        'title.unique'          => '标题已存在',
        'sort.number'  => '排序只能填写数字'
    ];
}